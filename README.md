# Quartet Sampling #
Quartet sampling can be a helpful analysis to conduct on large phylogenies as a measure of support. 

## Requirements ##
* RAxML: precompiled and you probably want the threaded version
* python: version 2

## Usage ##

## Contributors ##
* Stephen A. Smith
* Cody Hinchliff
- James B. Pease
- Joseph W. Brown
- Joseph F. Walker

