#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os


class ParamSet(dict):
    """Parameter container dictionary"""

    def setup(self, args, nleaves):
        """Setup main parameters"""
        # defaults
        default_mrsp = 0.5
        # Basic params
        self['verbose'] = args.verbose
        self['ignore_error'] = args.ignore_errors
        self['low_mem'] = args.low_mem
        self['retain_temp'] = args.retain_temp
        self['amino_acid'] = args.amino_acid
        if args.partitions is not None and args.genetrees is not None:
            raise RuntimeError("Cannot use -g (--genetrees)"
                               "and -q (--partitions) simultaneously")
        self['partitions_file_path'] = (os.path.abspath(args.partitions[0])
                                        if args.partitions is not None
                                        else None)
        self['using_partitions'] = self['partitions_file_path'] is not None
        self['genetrees_file_path'] = (os.path.abspath(args.genetrees[0])
                                       if args.genetrees is not None
                                       else None)
        self['using_genetrees'] = self['genetrees_file_path'] is not None
        self['raxml_executable'] = (args.raxml_executable[0] if
                                    args.raxml_executable is not None else
                                    'raxml')
        if self['using_genetrees'] is True and self['low_mem']:
            raise RuntimeError("Cannot use -g (--genetrees) and"
                               "--low-mem simultaneously")
        # This parameter sets a threshold for how close you need to be to the
        # total number of possible quartets to trigger exhaustive
        # sampling
        self['max_quartet_enumeration_threshold'] = 0.333333
        self['just_clade'] = args.clade is not None
        self['paup'] = bool(args.paup)
        self['nprocs'] = args.number_of_threads[0]
        print("setting the number of threads to {}".format(self['nprocs']))
        self['nreps'] = args.number_of_reps[0]
        print("setting the number of replicates to {}".format(self['nreps']))
        self['min_overlap'] = (args.min_overlap if
                               args.min_overlap is not None else 1)
        print("setting the minimum overlap to {}".format(self['min_overlap']))
        self['startk'] = (args.start_node_number[0]
                          if args.start_node_number is not None
                          else 1)
        self['stopk'] = (args.stop_node_number[0]
                         if args.stop_node_number is not None
                         else nleaves + 100)
        if self['stopk'] < self['startk']:
            raise RuntimeError("The start node number is higher"
                               "than the stop node number, "
                               "designating no nodes for processing.")
        # File Paths
        # print(args.temp_dir)
        self['temp_wd'] = (os.path.abspath(args.temp_dir[0])
                           if args.temp_dir is not None
                           else os.path.abspath("temp"))
        print("setting the temp working dir to {}".format(self["temp_wd"]))
        self['result_prefix'] = (args.result_prefix[0]
                                 if args.result_prefix is not None
                                 else "RESULT")
        self['results_dir'] = os.path.abspath(args.results_dir is not None and
                                              args.results_dir[0] or ".")
        self['tree_result_file_path'] = "{}/{}.labeled.tre".format(
            self['results_dir'], self['result_prefix'])
        self['score_result_file_path'] = "{}/{}.node.scores.csv".format(
            self['results_dir'], self['result_prefix'])
        self['figtree_file_path'] = "{}.figtree".format(
            self['tree_result_file_path'])
        self['freq_file_path'] = "{}.freq".format(
            self['tree_result_file_path'])
        self['qc_tree_file_path'] = "{}.qc".format(
            self['tree_result_file_path'])
        self['qd_tree_file_path'] = "{}.qd".format(
            self['tree_result_file_path'])
        self['qu_tree_file_path'] = "{}.qu".format(
            self['tree_result_file_path'])
        self['verbout'] = args.verbout
        if args.verbout is True:
            self['verbout_file_path'] = "{}/{}.verbout".format(
                self['results_dir'], self['result_prefix'])
            with open(self['verbout_file_path'], "w") as verbout:
                verbout.write("topo1,topo2,topo3,topou,qc,qd,qu\n")
        self['max_random_sample_proportion'] = default_mrsp
        if args.max_random_sample_proportion:
            self['max_random_sample_proportion'] = (
                args.max_random_sample_proportion)
            print("setting the maximum proportion of "
                  "possible quartets to be explored to: {}".format(
                      args.max_random_sample_proportion))
            if (args.max_random_sample_proportion >
                    default_mrsp):
                print("WARNING: for some alignments, the quartet "
                      "randomization procedure may take a long time to finish "
                      "(or fail) when max proportion of quartets to sample "
                      "is greater than {}".format(
                          default_mrsp))
        if args.lnlike_thresh:
            self['lnlikethresh'] = args.lnlike_thresh[0]
            print("setting the minimum lnL thresh to {}".format(
                self['lnlikethresh']))
        else:
            self['lnlikethresh'] = 0
        return ''

    def __str__(self):
        """Print all parameters in alphabetical order"""
        return ('\n'.join(sorted("{}={}".format(k, v)
                          for k, v in self.items())))


def read_config(configfilepath):
    args = []
    with open(configfilepath, 'r') as cfile:
        for line in [l.strip() for l in cfile.readlines()]:
            if len(line) < 1 or line[0] == '#':
                continue
            if '=' in line:
                elems = [d.strip() for d in line.split('=')]
            else:
                elems = [d.strip() for d in line.split()]
            args.extend(elems)
    return args
