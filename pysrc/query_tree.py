#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8
#
# This script is meant as a general template for Py3
#
# @author: James B. Pease
# @version: 1.0

import os
import sys
import argparse
# from tree_reader import read_tree_string
from tree_data import TreeData
from tree_utils import get_mrca
# from Bio import Phylo


def main(arguments=None):
    arguments = arguments if arguments is not None else sys.argv[1:]
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-t', '--tree', type=open, nargs=1)
    parser.add_argument('-d', '--data', type=os.path.abspath, nargs=1)
    # parser.add_argument("-n", '--nodes', nargs=1,
    #                    help=("comma-separated list of nodes to use"))
    parser.add_argument("-c", "--clade", nargs=1)
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-s", "--startk", type=int, default=0)
    parser.add_argument("-p", "--stopk", type=int)
    args = parser.parse_args(args=arguments)
    treedata = TreeData(args)
    params = {
        'starttime': 0,
        'startk': args.startk,
        'stopk': (args.stopk if args.stopk is not None else
                  treedata.nleaves + 100),
        'verbose': args.verbose,
        'temp_wd': os.path.curdir,
        'results_dir': os.path.curdir,
        'suppress_feedback': args.verbose is not True,
        }
    data = {}
    with open(args.data[0]) as datafile:
        firstline = True
        for line in datafile:
            if firstline is True:
                hdr = line.rstrip().split(',')[1:]
                firstline = False
            row = line.rstrip().split(',')
            data[row[0]] = row[1:]
    print("data read")
    # args.nodes = args.nodes[0].split(',')
    nodes = {}
    parent = {}
    k = 1
    ntotal = 0
    for xnode in treedata.tree.iternodes():
        k, _ = treedata.check_node(xnode, k, params)
        if xnode.label == '':
            xnode.label = str(k)
        xlabel = xnode.label
        nodes[xlabel] = xnode
        for xchild in xnode.children:
            parent[xchild] = xnode
        if xlabel not in data:
            continue
        xnode.data.update([(hdr[i], data[xlabel][i])for i in range(len(hdr))])
        if xnode.istip:
            ntotal += 1
    ret = ''
    print(ntotal, "total nodes in tree")
    print("Get MRCA data based on two nodes, separated by a comma")
    while ret not in ['quit', 'exit']:
        ret = input("Enter node names:")
        if ret in ['quit', 'exit']:
            sys.exit()
        try:
            ret = [x.strip() for x in ret.split(',')]
            if len(ret) == 1:
                mrca = nodes[ret[0]]
            else:
                mrca = get_mrca([nodes[x] for x in ret], treedata.tree)
            print("MRCA FOUND", mrca.label)
            print(mrca.data)

            def naround(num):
                return 'NA' if num == 'NA' else round(float(num), 2)

            print("SCORE={}/{}/{}".format(
                naround(mrca.data['qc']),
                naround(mrca.data['qd']),
                naround(mrca.data['qu']),
                ))
            print("TREE divided in to four groups:")

            def node_profile(xnode, grp):
                ntip = 0
                tipname = ''
                qtscores = []
                for x in xnode.iternodes():
                    if x.istip:
                        if tipname == '':
                            tipname = x.label[:]
                        ntip += 1
                        if 'qt' in x.data:
                            qtscores.append(float(x.data['qt']))
                print("GROUP{} has {} tips including {}, with meanQT={}".format(
                    grp, ntip, tipname, sum(qtscores)/len(qtscores)))
                return ntip
            ntip1 = node_profile(mrca.children[0], 0)
            ntip2 = node_profile(mrca.children[1], 1)
            mrca_parent = parent[mrca]
            if mrca_parent == treedata.tree:
                print("is root")
            else:
                for child in mrca_parent.children:
                    if child != mrca:
                        ntip3 = node_profile(child, 2)
            mrca_grandparent = parent[mrca_parent]
            if mrca_grandparent == treedata.tree:
                print("is root")
            else:
                ntip4 = ntotal - (ntip1 + ntip2 + ntip3)
                for child in mrca_grandparent.children:
                    if child != mrca_parent:
                        for xnode in child.iternodes():
                            if xnode.istip:
                                print("GROUP3 has {} tips including {}".format(
                                    ntip4, xnode.label))
                                break
        except Exception as e:
            print(e, sys.exc_info())
            print("error, try again or enter 'quit' to quit")
    return ''


if __name__ == '__main__':
    main()
