#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Convert the RESULT.node.csv file and tree file into a Figtree output"""

import sys
import os
import argparse
from tree_data import TreeData
from rep_data import DataStore


def main(arguments=None):
    """Main method for merge_output"""
    arguments = arguments if arguments is not None else sys.argv[1:]
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--nodedata', required=True, nargs=1,
                        help=("file containing paths of one or more"
                              "RESULT.node.score.csv files"))
    parser.add_argument('-t', '--tree', required=True, type=open,
                        nargs=1,
                        help="tree file in Newick format")
    parser.add_argument('-o', '--out', required=True,
                        nargs=1,
                        help="output file prefix")
    parser.add_argument("-c", "--clade", nargs=1)
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-s", "--startk", type=int, default=0)
    parser.add_argument("-p", "--stopk", type=int)
    args = parser.parse_args(args=arguments)
    scores = {}
    qtscores = {}
    qtreps = {}
    params = {
        'tree_result_file_path': "{}.tre".format(args.out[0]),
        'figtree_file_path': "{}.tre.figtree".format(args.out[0]),
        'freq_file_path': "{}.tre.freq".format(args.out[0]),
        'qc_tree_file_path': "{}.tre.qc".format(args.out[0]),
        'qd_tree_file_path': "{}.tre.qd".format(args.out[0]),
        'qu_tree_file_path': "{}.tre.qu".format(args.out[0]),
        'merged_file_path': "{}.nodes.scores.csv",
        }
    for fpath in params.values():
        if os.path.exists(fpath):
            raise RuntimeError("File {} already exists! Exiting...".format(
                fpath))
    treedata = TreeData(args)
    A = {
        'starttime': 0,
        'startk': args.startk,
        'stopk': (args.stopk if args.stopk is not None else
                  treedata.nleaves + 100),
        'verbose': args.verbose,
        'temp_wd': os.path.curdir,
        'results_dir': os.path.curdir,
        }
    params.update(A)
    mergedata = DataStore(params)
    filepaths = []
    with open(args.nodedata[0]) as nfile:
        for line in nfile:
            filepaths.append(line.strip())

    firstline = True
    for fname in filepaths:
        with open(fname, 'r') as infile:
            for line in infile:
                if firstline is True:
                    hdrs = line.rstrip().split(',')
                    #print(hdrs)
                    qtindex = hdrs.index('qt')
                    repindex = hdrs.index('num_replicates')
                    firstline = False
                    continue
                arr = line.rstrip().split(',')
                if len(arr) < 2:
                    continue
                if arr[qtindex] != 'NA':
                    qtscores[arr[0]] = qtscores.get(arr[0], 0.0) + (
                        float(arr[qtindex]) * float(arr[repindex]))
                    qtreps[arr[0]] = qtreps.get(
                        arr[0], 0.0) + float(arr[repindex])
                else:
                    if arr[0] in scores:
                        print("WARNING: {} appeared in two"
                              "separate input files".format(
                                  arr[0]))
                    scores[arr[0]] = dict([
                        (hdrs[i], arr[i]) for i in range(1, len(arr))])
    #print(scores)
    for xlabel in qtscores:
        print(xlabel, qtscores[xlabel], qtreps[xlabel])
        qtscores[xlabel] = "{:.2g}".format(
            qtscores[xlabel] / qtreps[xlabel])
    k = 1
    mergedata.write_headers(params['merged_file_path'])
    for xnode in treedata.tree.iternodes():
        k, _ = treedata.check_node(xnode, k, params)
        xlabel = xnode.label
        if xlabel in scores:
            xnode.label = xlabel[:]
            xnode.data['freq0'] = scores[xlabel]['freq0']
            xnode.data['qc_score'] = scores[xlabel]['qc']
            xnode.data['qd_score'] = scores[xlabel]['qc']
            xnode.data['qu_score'] = scores[xlabel]['qc']
            xnode.data['replicates'] = scores[xlabel]['num_replicates']
            entry = {'node_label': xlabel,
                     'freq0': scores[xlabel]['freq0'],
                     'qc': scores[xlabel]['qc'],
                     'qd': scores[xlabel]['qd'],
                     'qu': scores[xlabel]['qu'],
                     'num_replicates': scores[xlabel]['num_replicates'],
                     'diff': scores[xlabel]['diff'],
                     'notes': scores[xlabel]['notes']
                    }
            mergedata.write_entry(params['merged_file_path'], entry)
            entry = {}
    for xlabel in sorted(qtscores):
        entry = {'node_label': xlabel,
                 'qt': qtscores[xlabel],
                 'num_replicates': qtreps[xlabel]
                }
        mergedata.write_entry(params['merged_file_path'], entry)
        entry = {}
    treedata.write_scoretrees(params)
    treedata.write_figtree(params['figtree_file_path'], qtscores)
    return ''


if __name__ == '__main__':
    main()
